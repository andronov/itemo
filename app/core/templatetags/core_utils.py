from uuslug import slugify
from django import template

register = template.Library()


@register.filter(name='slugifys')
def slugifys(value):
    return slugify(value)
