# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import collections

from jsonfield import JSONField
from django.db import models

from accounts.models import UserProfile


class BaseModel(models.Model):
    is_active = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


"""
#
# Bids
#
"""

class Bids(BaseModel):
    CHOICE_TYPE = (
        (1, 'С сайта'),
        (2, 'Вручную')
    )
    CHOICE_TYPE_BID = (
        (1, 'Заявка'),
        (2, 'Звонок')
    )
    CHOICE_STATUS = (
        (1, 'Модерация'),
        (2, 'Одобрено'),
        (3, 'Брак')
    )
    user = models.ForeignKey(UserProfile, related_name='bids_user', blank=True, null=True)
    name_customer = models.CharField(max_length=255, blank=True, null=True)
    email_customer = models.EmailField(blank=True, null=True)
    phone_customer = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    extra_info = models.TextField(blank=True, null=True)

    type = models.IntegerField('Type', default=1, choices=CHOICE_TYPE, null=True)
    type_bid = models.IntegerField('Type bids', default=1, choices=CHOICE_TYPE_BID, null=True)

    md5 = models.CharField(max_length=5555, blank=True, null=True)

    http_refer = models.CharField(max_length=5555, blank=True, null=True)
    ip = models.CharField(max_length=5555, blank=True, null=True)
    url_path = models.CharField(max_length=5555, blank=True, null=True)
    utm_campaign = models.CharField(max_length=5555, blank=True, null=True)
    utm_medium = models.CharField(max_length=5555, blank=True, null=True)
    utm_term = models.CharField(max_length=5555, blank=True, null=True)
    utm_page = models.CharField(max_length=5555, blank=True, null=True)
    utm_phrase = models.CharField(max_length=5555, blank=True, null=True)


    commentary = models.TextField(max_length=1000, blank=True, null=True)


    send = models.BooleanField(default=False, blank=True)

    g_country = models.CharField(default='', max_length=12, blank=True, null=True)
    g_district = models.CharField(default='', max_length=500, blank=True, null=True)
    g_region = models.CharField(default='', max_length=500, blank=True, null=True)
    g_city = models.CharField(default='', max_length=500, blank=True, null=True)


    json = JSONField(verbose_name='Extra', null=True, blank=True, default='{}',
                      load_kwargs={'object_pairs_hook': collections.OrderedDict})

    from_lp_oxiv = models.BooleanField(default=False, blank=True)

    send_websocket = models.BooleanField(default=False, blank=True)

    def __unicode__(self):
        return str(self.id)
