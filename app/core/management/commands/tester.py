# -*- coding: utf-8 -*-
import datetime
import json
import time
import re
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from slugify import slugify

from geo.models import GeoBaseSite


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        print('START COMMAND')
        tester2()
        # start = time.time()
        # calculation_cities('')
        # print(time.time() - start)
        print('END COMMAND')


def tester2():
    for geo in GeoBaseSite.objects.all():
        slug = slugify(geo.region_name)
        geo.translate = slug
        geo.slug = slug
        geo.save()

def tester1():
    #for f in GeoBaseSite.objects.all():
    #    f.delete()
    tmp_filename = settings.TMP_PATH + '/result.txt'
    with open(tmp_filename, 'r') as f:
        js = json.load(f)
        for geo in js['geos']:
            id = geo['id']
            region_id = geo['region_id']
            parents_id = geo['parents_id']
            parent = geo['parent']
            region_name = geo['region_name']
            region_type = geo['region_type']
            priority = geo['priority']
            is_active = geo['is_active']
            print id
            print region_id
            print parents_id
            print parent
            print region_name
            print region_type
            print priority
            print is_active
            print "HHHH"

            pt = None
            if parent:
                pt = GeoBaseSite.objects.get(id=parent)
            g = GeoBaseSite.objects.get(id=id)
            g.parent = pt
            g.save()
