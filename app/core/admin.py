from django.contrib import admin

# Register your models here.
from core.models import Bids

class BidsAdmin(admin.ModelAdmin):
    search_fields = ['id']
    list_display = ('id', 'name_customer', 'email_customer', 'created',  'is_active', 'phone_customer',
                    'city', 'http_refer',)


admin.site.register(Bids, BidsAdmin)