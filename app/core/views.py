# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView
from django.views.generic.edit import FormMixin

from core.utils import never_ever_cache
from geo.models import GeoBaseSite


class HomePageView(TemplateView):
    template_name = 'core/home_with_city.html'

    def dispatch(self, *args, **kwargs):

        return super(HomePageView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()

        return super(HomePageView, self).render_to_response(context)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        GET = self.request.GET

        if 'itemojson' in GET:

            if 'get_cities' in GET:
                geo = GeoBaseSite.objects.filter(is_active=True).order_by('priority',
                                                                      'region_name')
                context['geo'] = geo
                html = render_to_string("core/extra/get-cities.html", context)
                return JsonResponse({'results': html}, safe=False)

            elif 'get_next_page' in GET:
                context['step'] = GET['step']
                context['name'] = GET['name']
                context['slug'] = GET['slug']
                html = render_to_string("core/page/step.html", context)
                return JsonResponse({'results': html}, safe=False)
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        return context


"""
#
# HomePageView with City "example itemo.ru/moskva"
#
"""

class HomePageCityView(DetailView):
    template_name = 'core/home_with_city.html'
    model = GeoBaseSite

    def dispatch(self, *args, **kwargs):

        return super(HomePageCityView, self).dispatch(*args, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        response = super(HomePageCityView, self).render_to_response(context, **response_kwargs)
        return response

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        return JsonResponse({'result': ''}, safe=False)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data()
        GET = self.request.GET

        if 'itemojson' in GET:

            if 'get_cities' in GET:
                geo = GeoBaseSite.objects.filter(is_active=True).order_by('priority',
                                                                      'region_name')
                context['geo'] = geo
                html = render_to_string("core/extra/get-cities.html", context)
                return JsonResponse({'results': html}, safe=False)

            elif 'get_next_page' in GET:
                context['step'] = GET['step']
                context['name'] = GET['name']
                context['slug'] = GET['slug']
                context['geo'] = self.object
                html = render_to_string("core/page/step.html", context)
                return JsonResponse({'results': html}, safe=False)

        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(HomePageCityView, self).get_context_data(**kwargs)
        obj = self.get_object()

        context['geo'] = obj
        return context


"""
#
# Transaction process (Прием заявок)
#
"""
@xframe_options_exempt
@method_decorator(csrf_exempt)
@method_decorator(never_ever_cache)
def transaction_process(request, pk=None):
    GET = request.GET
    print('transaction_process m', request.META)
    print('transaction_process g', request.GET)

    if 'proccess_oxiv' in GET:
        """
        landing = Landings.objects.get(pk=pk)
        try:
            bid = func_process_page_lead_form(request, landing)
            print('proccesss', bid)
            if bid.page.id == 2:

                return JsonResponse({'result': 'proccess', 'url': landing.url, 'id': bid.md5}, safe=False)
            else:
                return JsonResponse({'result': 'success', 'url': landing.url}, safe=False)
        except:
            print('proccesss', GET)
            print('Error')
            return JsonResponse({'result': 'failure', 'url': landing.url}, safe=False)
        """


    return render_to_response('handler/transaction_process.html', {'GET': GET})
