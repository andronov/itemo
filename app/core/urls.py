from django.conf.urls import url, include
from django.contrib import admin

from core.views import *

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='core_home'),

    url(r'^transaction/process/$', transaction_process, name='core_transaction_process'),

    url(r'^(?P<slug>[-\w]+)/$',  HomePageCityView.as_view(), name='core_home_city'),
]