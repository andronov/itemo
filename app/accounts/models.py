# -*- coding: utf-8 -*-
import hashlib
# from django.contrib.auth.models import AbstractUser
import collections
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, UserManager, PermissionsMixin
from django.contrib.postgres.forms import JSONField
from django.core.mail import send_mail, EmailMultiAlternatives
from django.db import models
from django.template.loader import render_to_string
from django.utils import six, timezone
from django.db.models.signals import post_save, pre_save
from datetime import timedelta, time
import datetime
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from geo.models import GeoBaseSite


class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """
    username = models.CharField(_('username'), max_length=50, unique=True,
                                help_text=_('Required. 50 characters or fewer.s'),
                                error_messages={
                                    'unique': _("A user with that username already exists."),
                                })
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    cities = models.ManyToManyField(GeoBaseSite, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def city(self):
        f = self.cities.first()
        if f:
            return f.region_name
        else:
            return ''

def _createPass():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:6]


class UserProfile(AbstractUser):
    is_customer = models.BooleanField(default=True)
    is_manager = models.BooleanField(default=False)

    phone = models.CharField(max_length=255, blank=True, null=True)
    topic = models.CharField(max_length=2255, blank=True, null=True)

    city = models.CharField(max_length=2255, blank=True, null=True)
    extra = models.TextField(blank=True, null=True)

    send = models.BooleanField(default=False, blank=True)
    send_websocket = models.BooleanField(default=False, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    """
    Users within the Django authentication system are represented by this
    model.

    Username, password and email are required. Other fields are optional.
    """

    def __unicode__(self):
        if self.first_name or self.last_name:
            full_name = '%s %s' % (self.first_name, self.last_name)
            return full_name.strip()
        else:
            return self.username

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    # get change balance ONLY email
    def get_change_password(self):
        password = _createPass()
        self.set_password(password)
        self.save()
        return str(password)

