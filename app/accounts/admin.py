from django.contrib import admin
from accounts.models import *


class UserProfileAdmin(admin.ModelAdmin):
    search_fields = ['id']
    list_display = ('id', 'username')

admin.site.register(UserProfile, UserProfileAdmin)