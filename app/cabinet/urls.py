from django.conf.urls import url, include
from django.contrib import admin

from cabinet.views import *

urlpatterns = [
    url(r'^$', HomePageCabinetView.as_view(), name='cabinet_home'),
]