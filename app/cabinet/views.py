from django.http import JsonResponse
from django.views.generic import TemplateView


class HomePageCabinetView(TemplateView):
    template_name = 'cabinet/home.html'

    def dispatch(self, *args, **kwargs):

        return super(HomePageCabinetView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()

        return super(HomePageCabinetView, self).render_to_response(context)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        GET = self.request.GET

        if 'itemojson' in GET:
                return JsonResponse({'results': ''}, safe=False)
        else:
            return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(HomePageCabinetView, self).get_context_data(**kwargs)
        return context