from django.contrib import admin
from geo.models import GeoBaseSite

class GeoBaseSiteAdmin(admin.ModelAdmin):
    search_fields = ['region_name']
    list_display = ('region_name',)

admin.site.register(GeoBaseSite, GeoBaseSiteAdmin)