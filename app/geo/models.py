# -*- coding: utf-8 -*-
from django.db import models

"""
#
#  База Городов Яндекс
#
"""
class GeoBaseSite(models.Model):
    region_id = models.BigIntegerField("RegionID", blank=True, null=True)
    parents_id = models.BigIntegerField("ParentID", blank=True, null=True)

    parent = models.ForeignKey("self", blank=True, null=True)

    region_name = models.CharField("RegionName", max_length=2250, null=True, blank=True)
    region_type = models.CharField("RegionType", max_length=2250, null=True, blank=True)

    slug = models.CharField("slug", max_length=2250, null=True, blank=True)
    translate = models.CharField("slug", max_length=2250, null=True, blank=True)

    priority = models.BigIntegerField("Priority", default=10000, null=True, blank=True)

    is_active = models.BooleanField(default=True)
    is_show = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created']


    def children(self):
        return GeoBaseSite.objects.filter(parents_id=self.region_id)
