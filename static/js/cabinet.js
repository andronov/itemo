var s,
Cabinet = {
    //base settings cabinet
    settings: {
        $body: $('body'),
        promoProgressBarStatus: [
            {'action': false, 'name': 'Сбор информации'},
            {'action': true, 'name': 'Расчет заявки'},
            {'action': true, 'name': 'Отправка заявки'},
            {'action': true, 'name': 'Назначение менеджера'},
            {'action': true, 'name': 'Ожидание звонка'}
        ],
        $modaldetaillead: $('#ModalCabinetDetailLead')
    },

    init: function() {
        s = this.settings;
        console.log('init', s);
        this.switchInit();
        this.run();

    },

    run: function() {
        var $this = this;

        // Open detail cabinet lead
        //=========================
        $(document).on('click', '.cabinet_lead', function(event) {
            event.preventDefault();
            event.stopPropagation();

            var id = $(this).data('id');
            $this.detailLead(id)
        });

        // Close detail cabinet lead
        //=========================
        $(document).on('click', '#ModalCabinetDetailLead .close-modal-close', function(event) {
            event.preventDefault();
            event.stopPropagation();

            $(this).parents('.modal').removeClass('active in');
            var $body = s.$body;
            $body.removeClass('modal-open');

        });
    },

    switchInit: function() {
        this.promoProgressBar()
    },

    detailLead: function(id) {
        console.log('detailLead', id);
        var $modaldetaillead = s.$modaldetaillead;
        var $body = s.$body;
        $body.addClass('modal-open');
        $modaldetaillead.addClass('active in');
    },

    promoProgressBar: function() {
        // Change title in progress bar
        //=========================
        var status = s.promoProgressBarStatus;
        var promo_lead = $('.cabinet_lead.promo_lead');
        var title = promo_lead.find('.progress-bar-title');


        function testPause(Pause) {

            setTimeout( function() {
                 $.each(status, function(index, value){
                    if(value.action){
                        value.action = false;
                        title.html(value.name);
                        testPause(5000);
                        return false;
                    }
                 });
            }, Pause );
        }

        testPause(5000);
    }
};


(function() {

  Cabinet.init();

})();