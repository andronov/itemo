$(document).ready(function($) {

    /* func no refresh page*/
    /*$(window).bind('popstate', function() {
        $.ajax({url:location.pathname, success: function(data){
            $('#content').html(data);
        }});
    });*/
    /* func no refresh page*/



    var $choicemodal = $('#ModalChoiceCityFillIn');
    var $body = $('body');

    /* callback button footer */
    function callbackButton() {
        function removeClass(classname, element) {
            var cn = element.className;
            var rxp = new RegExp("\\s?\\b" + classname + "\\b", "g");
            cn = cn.replace(rxp, '');
            element.className = cn;
        }

        function hasClass(className, target) {
            return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
        }

        var int = setInterval(function () {
            var call1 = document.getElementsByClassName('cbh-widget-phone')[0];
            var call2 = document.getElementsByClassName('cbh-widget-call')[0];
            if (hasClass('cbh-rotate-icon', call1)) {
                removeClass('cbh-rotate-icon', call1);
                call2.className += " cbh-rotate-icon";
            }
            else {
                removeClass('cbh-rotate-icon', call2);
                call1.className += " cbh-rotate-icon";
            }
        }, 2500);
    }
    //callbackButton();
    /* callback bbutton footer */

    /* hamburger */
    $('#hamburger').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        burgerTime();
    });
    function burgerTime() {
        var trigger = $('.hamburglar');
        trigger.addClass('hamburglar');
        trigger.toggleClass('is-open');
        trigger.toggleClass('is-closed');
    }

    /* end hamburger */

    /* main choice city */
    $(document).on('click', '#choicecity', function(event) {

        event.preventDefault();
        event.stopPropagation();
        console.log('choicecity', event);
        $choicemodal.modal('show');
    });
    /*  main choice city */

    /* filter popover */
    function funcOption(el) {
        $(el).on('click', function () {
            var txt = $(this).text();
            var list = $('.reg-panel-search-list');
            $.each($('.choice-elem'), function (i, item) {
                var z = $(item).data('name');
                if (z == txt) {
                    $(item).click();
                }
            });
            var popover = $('.popover_search_list');
            popover.css({'display': 'none'});
            $('.select2-search__field').val('')
        });
    }

    function funcSearch(city) {
        $('.select2-search__field').unbind().keyup(function () {
            var value = $(this).val();
            var pos = $(this).offset();
            var popover = $('.popover_search_list');
            var block = $('.reg-panel-search-list');
            var top = pos.top + $(this).height() + 2;
            popover.css({'top': top, 'left': pos.left, 'z-index': 2000});
            var list = $(popover.find('.p_search_list_results__options'));
            list.html(' ');

            var cts = [];
            var c = $('.ct-elem');
            if (city) {
                c = $('.ct-city')
            }

            var prt = $(this).parents('.steper');
            if (prt.length >= 1) {
                c = prt.find('.choice-elem');
            }


            $.each(c, function (i, item) {
                var z = $(item).data('name').toLowerCase().indexOf(value.toLowerCase());
                if (z != -1) {
                    //console.log($(this).data('name'));
                    cts.push($(this).data('name'));
                    if (list.find('.p_search_list_results__option').length <= 5) {
                        var ht = '<li class="p_search_list_results__option" role="treeitem" aria-selected="false">' + $(this).data('name') + '</li>'
                        list.append(ht);
                        funcOption(list.find('.p_search_list_results__option').last());
                    }
                }
            });

            var w = $('.panel-search').width();//+ 27;
            if (city) {
                w = $choicemodal.find('.panel-search').width()
            }
            if (prt.length >= 1) {
                w = $(prt.find('.panel-search')).width();
            }

            var block_p = $(popover.find('.p_search_list_dropdown'));
            block_p.css({'width': w + 'px'});

            if (cts.length > 0 && value.length > 0) {
                popover.css({'display': 'block'});
            }
            else {
                popover.css({'display': 'none'});
            }

        });
    }
    /* end filter popover */

    /* выбор города */

    $('.header-select-city').click(function () {
        var trigger = $(this);
        var el = $(trigger);
        var modals = $choicemodal;
        var arr = [];
        var prt = el.parents('.admin_user');
        var prt_td = el.parent();
        var block = $(modals.find('.panel-group'));
        $(modals.find('.panel-header-h')).html('Выберите регион');

        $.each(prt_td.find('.admin__user_geo'), function (i, item) {
            arr.push($(item).data('name'))
        });

        funcSearch('city');

        console.log(arr);

        $.get(location.href, {'itemojson': 'itemojson', 'get_cities': 'get_cities'})
            .done(function (data) {
                $(modals).find('.loader-choice').hide();
                $(modals).find('.modal-content').show();
                block.html(data.results);
                block.find('.ui-first-child').first().hide();

                var city = $('.header-select-text').data('name');
                var el = $('.choice-elem[data-name="' + city + '"]').parent();
                console.log('e', el);
                /*if (el.length <= 1) {
                    el.find('h2').css({'padding-left': '42px'}).html('<i style="position: absolute;top: 14px;left: 20px;" class="icon io-location" aria-hidden="true"></i> ' + city);
                    var pr = $('.choice-elem[data-name="Вся Россия"]').parent();
                    pr.after(el);
                }*/

                $(block.find('.choice-elem')).click(function () {
                    console.log('choice-elem', $(this).data('name'));
                    var n = $(this).data('name');
                    if (arr.indexOf(n) == -1) {
                        $('.header-select-text').html('г. ' + n);

                        window.history.pushState({path:'/'+ $(this).data('slug') +'/'},'','/'+ $(this).data('slug')+'/');

                        var id = prt.data('id');
                        modals.modal('hide');

                    }
                });
            });
        modals.modal('show');
    });
    /* выбор города */


    /* next page */
    $(document).on('click', '.next-page', function(event) {
        var e = $(this);
        var slug = e.data('slug'),
            step = e.data('step'),
            current_step = parseInt(step) - 1,
            name = e.data('name'),
            id = e.data('id'),
            pages = $('.pages-step-'+current_step);
        console.log('click', window.location.pathname);

        if(!$body.hasClass('page-'+slug)){

            $.get(location.href, {'itemojson': 'itemojson', 'get_next_page': 'get_next_page', 'step': step,
            'slug': slug, 'name': name})
            .done(function (data) {
                console.log('next', data);
                pages.after(data.results)


            });
            //console.log('.substr(-1)', window.location.pathname.substr(-1));
            var path = window.location.pathname +  slug;
            /*if(window.location.pathname == '/'){
                path = window.location.pathname + slug;
            }*/
            window.history.pushState({path:path},'', path);
            $body.addClass('page-'+slug).addClass('page-step-'+step);
            pages.addClass('no-page');

        }


    });
    /* next page */

    /* stepback */
    $(document).on('click', '.stepback-button', function(event) {
        var e = $(this);
        var stepback = e.parents('.page').data('step');
        var slugback = e.parents('.page').data('slug');
        console.log('click', window.location.pathname);
        $body.removeClass('page-step-' + stepback).removeClass('page-' + slugback);
        $('.pages-step-' + stepback).remove();
        $('.pages-step-' + (stepback-1)).removeClass('no-page');

        window.history.pushState({path:window.location.pathname.replace(slugback, '')},'', window.location.pathname.replace(slugback, ''));
    });
    /* stepback */


});